from django.shortcuts import render
from .models import Articulos
from django.http import HttpResponse


def inicio(request):
    return render(request, 'index.html')


def bienvenida(request):
    return render(request, 'bienvenida.html')


def busqueda_articulos(request):
    return render(request, 'busqueda_articulos.html')



def resultado_busqueda_articulos(request):
    if request.GET["art"]:
        art = request.GET["art"]
        articulo = Articulos.objects.filter(nombre__icontains=art)
        contexto = {
            'articulo': articulo,
            'query': art
        }
        return render(request, "resultados_busqueda.html", contexto)
    else:
        mensaje = "No hay información"
    return HttpResponse(mensaje)
