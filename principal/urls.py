from django.urls import path
from . import views

urlpatterns = [
    path('', views.inicio, name='inicio'),
    path('bienvenida/', views.bienvenida, name='bienvenida'),
    path('buscar_articulos/', views.busqueda_articulos, name='busquedaArt'),
    path('buscar/', views.resultado_busqueda_articulos, name='resultBusquedaArt'),
]